# Badge development

To develop code on the badge, you need to set up a development environment (using ESP IDF for the compiling and linking), and you need to make sure that when code is compiled you can flash the badge.

## 0. Prerequisites
The following instructions are based on the assumption you are using a Linux system.

Known issues:
- WSL (Windows Subsystem for Linux) will not work, since it is unable to use the USB device.
- MacOS can't easily map USB inside the container.

All these issues can be worked around by installing ESP IDF plugin on VScode and choosing v5.1.2. It's also faster to set up.

## Connecting your device
In a terminal, do
```
sudo tail -f /var/log/syslog | grep tty
```
<<<<<<< HEAD
=======

or
>>>>>>> b8991d4612dcdf280995c9aa99ba92e97b3e8d52

depending on your distro you might prefer to use

```
sudo dmesg -w | grep tty
```

Switch your badge on, then connect it to your machine.

Terminal should show some lines giving you a hint at which unix device you will be able to reach your badge. Typically, this would be something like `/dev/ttyACM0`, use it instead of `[PORT]` in the following istructions.

<<<<<<< HEAD
## Using ESP IDF natively
=======
## 2. Checking out the repo

```shell
$ git clone https://gitlab.com/tidklaas/hip-badge
$ git submodule update --init --recursive
$ cd hip-badge/firmware/blinkenlights
```

## 3. Using ESP IDF natively or containerized

### ESP IDF in a container

```shell
sudo docker run -i -t -v $PWD:/project -w /project --device [/dev/PORT] espressif/idf:v5.1.2
```

### ESP IDF Local installation
>>>>>>> b8991d4612dcdf280995c9aa99ba92e97b3e8d52

Development is done on the v5.1.x branch of the ESP-IDF. If you have not
installed the ESP-IDF, please follow the excellent instructions at
https://docs.espressif.com/projects/esp-idf/en/v5.1.2/esp32c3/get-started/index.html

If you already have an installed ESP IDF, check out the latest v5.1.x tag:

```
$ cd $IDF_PATH
$ git fetch
<<<<<<< HEAD
$ git checkout v4.4.3
```

(at this point you might have some residual files lying around in components/
 and examples/. You should very carefully inspect each and everyo NOOOO!!!!!!
 Nuke them from orbit: rm -rf components/ examples/; git checkout .)

```
=======
$ git checkout v5.1.2
>>>>>>> b8991d4612dcdf280995c9aa99ba92e97b3e8d52
$ git submodule update --init --recursive
$ bash install.sh
```
### ESP IDF VSCode plugin installation

Start from step 3 https://github.com/espressif/vscode-esp-idf-extension/blob/master/docs/tutorial/install.md

Select open folder and open blinkenlighs subdirectory of repo
At bottom of screen in status line select connected device it will be named something like `/dev/cu.usbmodem14011` depends on OS it will take some time for a pop up in searchbox to appear, wait for it and select device.

Next choose device target click on esp32 near device select at bottom of screen, wait a bit and select esp32c3

That should do the trick, run menuconfig a cogwheel icon to create initial configuration

Fire icon - build, flash, monitor 

## Use ESP IDF in a docker image

You can also use a docker version of it. Create this little shell script, which calls `idf.py` inside docker instead of directly (remember to replace `PORT` by the port that your badge appears at):

(idf.py)
```
#!/bin/bash
# as per https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-guides/tools/idf-docker-image.html
sudo docker run -v $PWD:/project -w /project --device PORT espressif/idf:release-v4.4 idf.py $*
```

Call this script instead of `idf.py` later on in the documentation.

## Checking out and modifying the firmware

To get started with your HiP Badge on a computer with a ESP IDF installation:

```
$ git clone https://git.sr.ht/~rcs/HiP-Badge
$ cd HiP-Badge/firmware
$ git submodule update --init --recursive
$ . /path/to/your/esp-idf/export.sh # this does not seem to be necessary nor effective
$ cd blinkenlights
$ idf.py set-target esp32c3
$ idf.py menuconfig --help (appearance with --style)
$ idf.py menuconfig (generates a sdkconfig file)
```

You should now be setup for development. The main file with blinkenlight is located in `main/hipbadge.c`. Make the changes you need, then...

<<<<<<< HEAD
```
=======
If buttons do not work according to [documentation](documents/README.md), depending on your badge version:

### 1 gen
(That's distributed on the HiP conference)

### 2 and 3 gen

Buttons assignment
9 -> 8
10 -> 9

Hit `S` to save, then build and re-flash as follows.

## 6. (Optional) Re-flashing firmware

You should now be set up for development. The main file with blinkenlight is located in `main/hipbadge.c`. Make the changes you need, then

```shell
>>>>>>> b8991d4612dcdf280995c9aa99ba92e97b3e8d52
$ idf.py build
```
and if that threw no errors, connect your device and do
```
$ idf.py -p PORT flash # usually, PORT would be /dev/ttyACM0
```
If this did not work, you can try to do
```
$ idf.py -p PORT erase-flash
$ idf.py -p PORT erase-otadata
$ idf.py -p PORT flash
```

Right after flashing, your device should reboot and operate with your changes.

## Tinkering with secure element

First you need to solder the secure element (ATECC608A) on REL_0.8.10 SE version or later

Install esp-cryptoauthlib, from blinkenlights folder
using this tutorial: https://github.com/espressif/esp-cryptoauthlib#how-to-use-esp-cryptoauthlib-with-esp-idf
Note: in case of errors like I2C_NUM_1 missing, install nano in docker container:
apt-get update
apt-get install nano

then change nano ./components/esp-cr
yptoauthlib/cryptoauthlib/third_party/hal/esp32/hal_esp32_i2c.c 
comment case with I2C_NUM_1 and it should compile.

Use esp_cryptoauth_utility to configure i2c pins and other ATECC608A parameters:
https://github.com/espressif/esp-cryptoauthlib/blob/master/esp_cryptoauth_utility/README.md

